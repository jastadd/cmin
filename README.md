# CMin - JastAdd C language demo

This is a small demo compiler for a C-like language. The compiler outputs plain C code.

The language is only partially complete. The main difference to plain C is that
structs have member functions.

To follow the tutorial for project: see Tutorial.pdf, or go to
[tiny.cc/jastadd_cmin][1].

## Building and Running

To build CMin you need a working Java environment (7 or later).

To get started, first build the compiler:

    ./gradlew jar


(Note: use `gradlew.bat` on Windows.)

This should generate a file named `compiler.jar` in the project root directory.
Try running it:

    java -jar compiler.jar testfiles/run/gcd.test


This should print some C code.

## Offline Building

Gradle builds normally need a network connection. However, we have prepared
a distribution of this project that can be built offline.

It will be made available during the workshop.  To build with the offline
version, add `--offline` to all `./gradlew` commands.


## File Overview

Files you might want to edit:

* `testfiles/...` - Integration test cases.
* `src/scanner/cmin.lex` - Token definitions.
* `src/parser/cmin.parser` - Parsing grammar.
* `src/jastadd/` - JastAdd aspects and abstract grammar files.
* `src/jastadd/CMin.ast` - Abstract grammar.
* `src/jastadd/CGen.jrag` - Code generation.
* `src/jastadd/Problems.jrag` - Error checking/reporting.
* `src/jastadd/TypeAnalysis.jrag` - Typing rules.
* `src/jastadd/NameLookup.jrag` - Name binding rules.
* `src/jastadd/FunLookup.jrag` - Function name binding rules.

Files that you won't need to edit:

* `build.gradle` - Build script.
* `src/main/` - Main compiler class.
* `src/test/` - Test harness.


## Testing

Testfiles are organized into categories in the `testfiles` directory.

You can run all tests by typing `./gradlew testAll`. This will include some
runtime tests which require GCC to compile generated C code. If you don't have
GCC installed, you can skip the runtime tests by instead running `./gradlew
test`.


## Editing

You can edit the lexer, parser, or semantic rules of the language.

See the [presentation slides][1].

[1]: http://tiny.cc/jastadd_cmin
