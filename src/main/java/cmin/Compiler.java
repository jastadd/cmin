package cmin;

import beaver.Parser.Exception;
import cmin.ast.ASTState;
import cmin.ast.Problem;
import cmin.ast.Program;
import cmin.parser.CMParser;
import cmin.scanner.CMScanner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Compiler {
  // For viewing the AST with DrAST:
  public static Object DrAST_root_node = null;

  public static void main(String[] args) throws IOException, Exception {
    if (args.length == 0) {
      System.err.println("Usage: Compiler <FILE>");
      System.exit(1);
    }

    String filename = null;
    Set<String> options = new HashSet<>();
    for (String arg : args) {
      if (arg.equals("--dump")) {
        options.add(arg);
      } else if (arg.startsWith("--")) {
        System.err.println("Error: unknown option " + arg);
        System.exit(1);
      } else if (filename != null) {
        System.err.println("Error: only one filename can be specified");
        System.exit(1);
      } else {
        filename = arg;
      }
    }

    try {
      CMScanner scanner = new CMScanner(new FileReader(filename));
      CMParser parser = new CMParser();
      Program program = (Program) parser.parse(scanner);
      program.sourceFile = filename;
      if (options.contains("--dump")) {
        program.dumpTree(System.out);
      }
      DrAST_root_node = program;
      if (!program.problems().isEmpty()) {
        System.err.println();
        System.err.println("Errors: ");
        for (Problem e : program.problems()) {
          System.err.println(e);
        }
      } else {
        Collection<String> code = program.ccode();
        for (String line : code) {
          System.out.println(line);
        }
      }
    } catch (FileNotFoundException e) {
      System.out.println("File not found: " + filename);
      System.exit(1);
    }
  }
}
