package cmin.scanner;

import beaver.Symbol;

import cmin.parser.CMParser.Terminals;
import cmin.parser.CMParser.SyntaxError;

%%

%public
%final
%class CMScanner
%extends beaver.Scanner

%type Symbol
%function nextToken

%yylexthrow beaver.Scanner.Exception

%unicode
%line %column

%{
  private Symbol sym(short id) {
    return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  int stringStartLine, stringStartColumn;
  StringBuilder stringBuilder = new StringBuilder();
  int commentDepth = 0;

  private void startString() {
    yybegin(STRING);
    stringStartLine = yyline + 1;
    stringStartColumn = yycolumn + 1;
    stringBuilder.setLength(0);
  }

  private Symbol stringEnd() {
    yybegin(YYINITIAL);
    String literal = stringBuilder.toString();
    int length = literal.length() + 2;
    return new Symbol(Terminals.STRING, stringStartLine, stringStartColumn, length, literal);
  }

  private void error(String message) {
    throw new SyntaxError(message);
  }

  private void enterComment() {
    if (commentDepth == 0) {
      yybegin(COMMENT);
    }
    commentDepth += 1;
  }

  private void leaveComment() {
    commentDepth -= 1;
    if (commentDepth == 0) {
      yybegin(YYINITIAL);
    }
  }
%}

eol = \n|\r
input_char = [^\r\n]

whitespace = [ ] | \t | \f | {eol}

eol_comment = "//" {input_char}* {eol}?

digit = [0-9]
num = {digit}+
id = [_a-zA-Z][_a-zA-Z0-9]*

string_char = [^\r\n\"]

line_terminator = \n|\r|\r\n

%state STRING
%state COMMENT

%%

<YYINITIAL> {

  {whitespace} { }
  {eol_comment} { }

  "if"         { return sym(Terminals.IF); }
  "else"       { return sym(Terminals.ELSE); }
  "while"      { return sym(Terminals.WHILE); }
  "return"     { return sym(Terminals.RETURN); }
  "break"      { return sym(Terminals.BREAK); }
  "void"       { return sym(Terminals.VOID); }
  "int"        { return sym(Terminals.INT); }
  "byte"       { return sym(Terminals.BYTE); }
  "bool"       { return sym(Terminals.BOOL); }
  "true"       { return sym(Terminals.TRUE); }
  "false"      { return sym(Terminals.FALSE); }
  "struct"     { return sym(Terminals.STRUCT); }
  "new"        { return sym(Terminals.NEW); }

  "@"          { return sym(Terminals.AT); }
  "="          { return sym(Terminals.EQ); }
  "<"          { return sym(Terminals.LT); }
  ">"          { return sym(Terminals.GT); }
  "=="         { return sym(Terminals.EQEQ); }
  "<="         { return sym(Terminals.LTEQ); }
  ">="         { return sym(Terminals.GTEQ); }
  "!="         { return sym(Terminals.NOTEQ); }
  "+"          { return sym(Terminals.ADD); }
  "-"          { return sym(Terminals.SUB); }
  "*"          { return sym(Terminals.MUL); }
  "/"          { return sym(Terminals.DIV); }
  "%"          { return sym(Terminals.MOD); }
  "&"          { return sym(Terminals.AND); }
  "&&"         { return sym(Terminals.ANDAND); }
  "||"         { return sym(Terminals.OROR); }
  "!"          { return sym(Terminals.NOT); }
  ":"          { return sym(Terminals.COLON); }

  "."          { return sym(Terminals.DOT); }
  "..."        { return sym(Terminals.ELLIPSIS); }

  "["          { return sym(Terminals.LBRACKET); }
  "]"          { return sym(Terminals.RBRACKET); }

  "("          { return sym(Terminals.LPAREN); }
  ")"          { return sym(Terminals.RPAREN); }

  "{"          { return sym(Terminals.LBRACE); }
  "}"          { return sym(Terminals.RBRACE); }

  ";"          { return sym(Terminals.SEMICOLON); }
  ","          { return sym(Terminals.COMMA); }

  "/*"         { enterComment(); }

  \"           { startString(); }
}
