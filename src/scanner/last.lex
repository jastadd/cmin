// These are in a separate file because they need to be the last token definitions.
<YYINITIAL> {
  {id}         { return sym(Terminals.ID); }

  {num}        { return sym(Terminals.NUM); }
}

<COMMENT> {
  "/*"         { enterComment(); }
  "*/"         { leaveComment(); }
  [^]          { }
}

<STRING> {
  \"                { return stringEnd(); }
  {string_char}+    { stringBuilder.append(yytext()); }
  {line_terminator} { error("unterminated string at end of line"); }
}
[^]            { error("Illegal character <" + yytext() + ">"); }
<<EOF>>        { return sym(Terminals.EOF); }
