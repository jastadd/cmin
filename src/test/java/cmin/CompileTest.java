package cmin;

import beaver.Parser;
import cmin.ast.Problem;
import cmin.ast.Program;
import cmin.parser.CMParser;
import cmin.scanner.CMScanner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

@RunWith(Parameterized.class)
public class CompileTest {
  private final File file;

  public CompileTest(File file, String filename) {
    this.file = file;
  }

  /** Build the list of test parameters (test input files). */
  @Parameterized.Parameters(name="{1}")
  public static Iterable<Object[]> getTests() {
    Collection<Object[]> tests = new LinkedList<>();
    addTests(tests, "testfiles/name", "name");
    addTests(tests, "testfiles/syntax", "syntax");
    addTests(tests, "testfiles/type", "type");
    return tests;
  }

  private static void addTests(Collection<Object[]> tests, String dirPath, String category) {
    File testDir = new File(dirPath);
    if (!testDir.isDirectory()) {
      throw new Error("Could not find the test directory '" + testDir + "'");
    }
    File[] files = testDir.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.getName().endsWith(".test")) {
          tests.add(new Object[] {file, category + "/" + file.getName()});
        }
      }
    }
  }

  @Test
  public void runTest() throws IOException, Parser.Exception {
    List<String> expectedLines = getExpectedLines();
    try (FileReader reader = new FileReader(file)) {
      CMScanner scanner = new CMScanner(reader);
      CMParser parser = new CMParser();
      Program program = (Program) parser.parse(scanner);
      program.sourceFile = "<test>";
      List<String> problems = program.problems().stream().map(Problem::toString)
          .collect(Collectors.toList());
      assertThat(problems).containsExactlyElementsIn(expectedLines);
    }
  }

  public List<String> getExpectedLines() throws IOException {
    List<String> expected = new LinkedList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      boolean addLines = false;
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        } else if (addLines) {
          if (line.startsWith("*/")) {
            break;
          }
          expected.add(line);
        } else if (line.startsWith("/*EXPECT")) {
          addLines = true;
        }
      }
    }
    return expected;
  }
}
